
# coding: utf-8

# In[10]:


import os
import numpy as np 
from PIL import Image
import glob#importing the library


# In[33]:


train_data_set = []
train_data_val = []

path = '/home/breezy/Documents/datas/'  #giving the path to directory
i = 0
for dire in os.listdir(path):
    paths = path + dire;
    val = i 
    print('in ',dire)
    i = i + 1
    for filename in glob.glob(os.path.join(paths, '*.png')):
        img = Image.open(filename)
        img = img.resize((32, 32), Image.ANTIALIAS)
        arr = np.array(img)
        train_data_set.append(arr)
        train_data_val.append(val)
train_data_set = np.array(train_data_set)


# In[35]:


test_data = Image.open('/home/breezy/Documents/datas/output.png')
test_data = test_data.resize((32, 32), Image.ANTIALIAS)
test_data_arr = np.array(test_data)

distance = []
#predict which
for imgs_arr in train_data_set:
    temp_distance_arr = abs(test_data_arr) - abs(imgs_arr)
    distance.append(np.sum(imgs_arr))
    


# In[36]:


print(distance)


# In[38]:


minimum = np.amin(distance)


# In[39]:


print(minimum)


# In[42]:


a = distance.index(minimum)


# In[43]:


print(train_data_val[a])

