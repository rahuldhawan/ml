import os
import operator
import matplotlib.pyplot as plt

my_eng_dict = {}
my_fre_dict = {}

path = '/home/breezy/my_eng_data'
with open (path,'r') as file_open_curr:
	data = file_open_curr.readlines()

for line in data:
	line = line.rstrip()
	words = line.split(' ')

	for word in words:
		if(len(word)>0):
			if (my_eng_dict.has_key(word)):
				val = my_eng_dict[word]
				my_eng_dict[word] = val + 1
			else:
				my_eng_dict[word] = 1

path_1 = '/home/breezy/my_fre_data'
with open (path_1,'r') as file_open_curr:
	data_1 = file_open_curr.readlines()

for line in data_1:
	line = line.rstrip()
	words_1 = line.split(' ')

	for word in words_1:
		if(len(word)>0):
			if (my_fre_dict.has_key(word)):
				val = my_fre_dict[word]
				my_fre_dict[word] = val + 1
			else:
				my_fre_dict[word] = 1

sorted_eng_dic = sorted(my_eng_dict.items(), key=operator.itemgetter(1))
sorted_fre_dic = sorted(my_fre_dict.items(), key=operator.itemgetter(1))

y_e = []
y_f = []
x = []

for i in range(500):
	a_e = sorted_eng_dic[(len(sorted_eng_dic))-i-1]
	a_f = sorted_fre_dic[(len(sorted_fre_dic))-i-1]
	y_e.append(a_e[1])
	y_f.append(a_f[1])
	x.append(i) 

axes = plt.gca()
axes.set_xlim([0,200])

plt.plot(x,y_e)
plt.show()


