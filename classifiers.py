
# coding: utf-8

# In[17]:


from sklearn import datasets
import numpy as np
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
from sklearn.naive_bayes import MultinomialNB
from sklearn import tree


# In[18]:


iris = datasets.load_iris()
iris_X = iris.data
iris_y = iris.target
np.random.seed(0)
indices = np.random.permutation(len(iris_X))
iris_X_train = iris_X[indices[:-10]]
iris_y_train = iris_y[indices[:-10]]
iris_X_test  = iris_X[indices[-10:]]
iris_y_test  = iris_y[indices[-10:]]


# In[19]:


def naive_bayes():
    gnb = MultinomialNB()
    gnb.fit(iris_X_train,iris_y_train)
    y_pred = gnb.predict(iris_X_test)
    acc =  accuracy_score(iris_y_test, y_pred)
    return(acc)


# In[20]:


def decision_tree():
    dec_tr = tree.DecisionTreeClassifier()
    dec_tr.fit(iris_X_train,iris_y_train)
    y_pred = dec_tr.predict(iris_X_test)
    acc =  accuracy_score(iris_y_test, y_pred)
    return(acc)


# In[21]:


def k_nearest():
    knn = KNeighborsClassifier()
    knn.fit(iris_X_train,iris_y_train)
    y_pred = knn.predict(iris_X_test)
    acc = accuracy_score(iris_y_test, y_pred)
    return(acc)


# In[22]:


def svm_simple():
    svm_model = svm.SVC()
    svm_model.fit(iris_X_train,iris_y_train)
    y_pred = svm_model.predict(iris_X_test)
    acc = accuracy_score(iris_y_test, y_pred)
    return(acc)


# In[24]:


nb_acc = naive_bayes()
print(nb_acc)
knn_acc = k_nearest()
print(knn_acc)
svm_simple_acc = svm_simple()
print(svm_simple_acc)
dec_tre_acc = decision_tree()
print(dec_tre_acc)

